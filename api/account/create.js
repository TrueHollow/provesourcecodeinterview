const Account = require('../../models/account/Account');

module.exports = async function(req, res, next) {
	if (req.method !== 'POST') return res.status(405).set('Allow', 'POST').send({err: 'invalid method, required POST'});
	const {email, name, age} = req.body;
	const exist = await Account.findOne({email: email}, '_id');
	if (exist) return res.status(409).send({err: 'email already exists'});
	const account = new Account({email, name, age});
	try {
		await account.save();
		return res.send({message: 'success'});
	} catch (e) {
		console.error(e);
		return res.status(500).send({err: 'internal error'});
	}
};