const Notification = require('../../models/notification/Notification');

module.exports = async function(req, res, next) {
    if (req.method !== 'GET') return res.status(405).set('Allow', 'GET').send({err: 'invalid method, required GET'});
    const {accountId} = req.query;
    const result = await Notification.findOne({accountId: accountId}, 'accountId name color -_id');
    if (result) return res.send(result.toJSON());
    return res.status(404).send({err: 'notification with this accountId wasn\'t found'});
};