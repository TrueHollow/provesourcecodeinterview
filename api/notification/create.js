const Notification = require('../../models/notification/Notification');

module.exports = async function(req, res, next) {
    if (req.method !== 'POST') return res.status(405).set('Allow', 'POST').send({err: 'invalid method, required POST'});
    const {accountId, name, color} = req.body;

    const notification = new Notification({accountId, name, color});
    try {
        await notification.save();
        return res.send({message: 'success'});
    } catch (e) {
        console.error(e);
        return res.status(500).send({err: 'internal error'});
    }
};