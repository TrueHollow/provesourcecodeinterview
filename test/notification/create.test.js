const chai = require('chai');
chai.use(require('chai-http'));
const expect = require('chai').expect;
const app = require('../../app');

const Notification = require('../../models/notification/Notification');

describe('POST /notification/create', () => {

    beforeEach(async () => {
        await Notification.remove();
    });

    it('should add notification to database', async() => {
        const data = {
            accountId: 'some_test_id',
            name: 'Testo',
            color: '#ababab'
        };
        let res = await chai.request(app).post('/notification/create').send(data);
        expect(res).to.have.status(200);

        const notification = await Notification.findOne();
        expect(notification).to.exist;
    });

});