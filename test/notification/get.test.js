const chai = require('chai');
chai.use(require('chai-http'));
const expect = require('chai').expect;
const app = require('../../app');

const Notification = require('../../models/notification/Notification');

describe('POST /notification/create', () => {

    beforeEach(async () => {
        await Notification.remove();
    });

    it('should add notification to database and test get method', async() => {
        const data = {
            accountId: 'some_test_id',
            name: 'Testo',
            color: '#ababab'
        };
        let res = await chai.request(app).post('/notification/create').send(data);
        expect(res).to.have.status(200);

        res = await chai.request(app).get('/notification/get').query({accountId: data.accountId});
        expect(res).to.have.status(200);

        expect(res.body).to.eql(data);
    });

});